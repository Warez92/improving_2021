$(".owl-carousel").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  dots: false,
  responsive: {
    0: {
      items: 2,
    },
    768: {
      items: 3,
    },
    1440: {
      items: 4,
    },
  },
});

$(".owl-nav").appendTo(".sliderNav");

/* Push the body and the nav over by 285px over */
$(".menuIkonBurger").click(function () {
  $("#slideInMobil").animate(
    {
      left: "0px",
    },
    200
  );

  $("body").animate(
    {
      left: "250px",
    },
    200
  );
});

/* Then push them back */
$(".icon-close").click(function () {
  $("#slideInMobil").animate(
    {
      left: "-250px",
    },
    200
  );

  $("body").animate(
    {
      left: "0px",
    },
    200
  );
});

// Toggle kurv
$(".menuIkonKurv").click(function () {
  $("#kurvSlideDown").slideToggle({});
});

$(".dropdown-toggle").click(function () {
  $(".dropdown-menu").slideToggle({});
});
